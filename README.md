# Tour of the Tidyverse

This workshop will take you on a tour of the tidyverse, the most influential and important group of R packages out there.

## Outline
* What the tidyverse is and why you should use it 
* What tools the tidyverse has to offer 
* A look at some of the core functionality on offer
